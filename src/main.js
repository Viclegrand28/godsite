// import Vue from 'vue'
// import App from './App.vue'

// Vue.config.productionTip = false

// new Vue({
//   render: h => h(App),
// }).$mount('#app')

// IMPORTS

// System requierment import
import Vue from 'vue'
import App from './App.vue'
// import store from './store/store'
// import axios from 'axios'

// Date component prototype formater import
import moment from 'moment'


// BootstrapVue and Bootstrap import
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// axios
import axios from 'axios'

//jwt decoder
// import jwtDecode from 'jwt-decode'

import VueAxios from 'vue-axios'
//VueRouter component import
// import VueRouter from 'vue-router'

// import jwtDecode from 'jwt-decode'

// System routes import
// import routes from './router/routes'

// import i18n from './i18n'

import VueMq from 'vue-mq'
// USES

// responsive
Vue.use(VueMq, {
  breakpoints: {
    sm: 450,
    md: 1250,
    lg: Infinity,
  }
})

// use axios
Vue.use(VueAxios,axios)
axios.defaults.baseURL = 'http://localhost:3000/'
// https://watcher-uy1.herokuapp.com/
// http://localhost:3000/
// use prototype of date formater
Vue.prototype.moment = moment


// Use BootstrapVue and Bootstrap components
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)


// use VueRouter component and jwt
// Vue.use(VueRouter);


// use System routes
// const router = new VueRouter({mode:'history',hash: false,routes})

// router.beforeEach((to, from, next) => {
//   if (to.name == 'student' && (localStorage.getItem('usertoken')===null ) ) next({ name: 'signin' })
//   if (to.name == 'stepm1' && (localStorage.getItem('usertoken')===null ) ) next({ name: 'signin' })
//   if (to.name == 'stepi1' && (localStorage.getItem('usertoken')===null ) ) next({ name: 'signin' })
//   if (to.name == 'stepc1' && (localStorage.getItem('usertoken')===null ) ) next({ name: 'signin' })
//   if (to.name == 'certmethode' && (localStorage.getItem('usertoken')===null ) ) next({ name: 'signin' })
//   // if the user is not authenticated, `next` is called twice
//   next()
// })
// filters
Vue.filter('money', function (value) {
	return value+" XAF"
})

// SETTINGS

// setting off production Tips
Vue.config.productionTip = false



// INSTALLATION
new Vue({
    // router,
    // store,
    // i18n,

    // axios,
    render: h => h(App)
}).$mount('#app')